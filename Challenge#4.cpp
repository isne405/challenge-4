#include<iostream>
#include<vector>
#include<ctime>
#include<cstdlib>
#include<cmath>
#include<iomanip>

using namespace std;
int NumElem(vector<int> );
int SumElem(vector<int> );
void sort(vector<int> &);
int Highest(vector<int> );
int Lowest(vector<int> );
float mean(vector<int> );
float median(vector<int> );
int Mode(vector<int> );
int StandardDeviation(vector<int> );
int OddNumber (vector<int> );
int EvenNumber(vector<int> );
void COUTVec(vector<int> );


int main(){
	vector <int> VecTest;
	
	srand(time(0));
	
	int size = rand()%101 + 50 ;
	for(int i = 0 ; i < size ; i++){
		VecTest.push_back(rand()%101);
	}
	sort(VecTest);
	
	cout << "Number Of Element : " << NumElem(VecTest) << endl;
//	cout << "Sum of all Element : " << SumElem(VecTest) << endl;
//	cout << "Highest Value is : " << Highest(VecTest) << endl;
//	cout << "Lowest value is : " << Lowest(VecTest) << endl;
//	cout << "Mean of the vector is : " << mean(VecTest) << endl;
	cout << "Median is : " << median(VecTest) << endl;
//	cout << "Mode is : " << Mode(VecTest) << endl;
//	cout << "Standard Deviation is : " << StandardDeviation(VecTest) << endl;
//	cout << "Odd numbers amount : " << OddNumber(VecTest) << endl;
//	cout << "Even numbers amount : " << EvenNumber(VecTest) << endl;
//	cout << "Hole Vector : " ;
	COUTVec(VecTest);
	cout << endl;
	
	return 0;
}

int NumElem(vector<int> VecTest){ // Retunr Vector size
	return VecTest.size(); 			//Big - O : 1
}

int SumElem(vector<int> VecTest){ //Find summary of vector
	int sum = 0;					//Big - O : N
	for(int i = 0 ; i < VecTest.size() ; i++){
		sum += VecTest.at(i);
	}
	return sum;
}


void sort(vector<int> &VecTest){ // sort since it's start why not?   
	int temp;						//Big - O : N
	for(int i = 0 ; i < VecTest.size() ; i++){
		for(int j = 0 ; j < VecTest.size() ; j++){
			if(VecTest.at(j) > VecTest.at(i)){
				temp = VecTest.at(i);
				VecTest.at(i) = VecTest.at(j);
				VecTest.at(j) = temp;
			}	
		} 
	}
}

int Highest(vector<int> VecTest){ // Find Highest value
	int Highest = VecTest.at(VecTest.size() - 1);//Big - O : N
	for(int i = 1 ; i < VecTest.size() ; i++){
		if(Highest < VecTest.at(i)){
			Highest = VecTest.at(i);
		}
	return Highest;
	}
}	

int Lowest(vector<int> VecTest){ // Find Lowest value
	int Lowest = VecTest.at(0);// Big - O : N
	for(int i = 1 ; i < VecTest.size() ; i++){
		if(Lowest > VecTest.at(i)){
			Lowest = VecTest.at(i);
		}
	}
	return Lowest;
}

float mean(vector<int> VecTest){ // Find mean
	float sum = SumElem(VecTest); //Big - O : N
	
	return sum / VecTest.size();
}

float median(vector<int> VecTest){ //Fine median 
	int MidSearch = VecTest.size() / 2;//Big - O : 1
	float median ;				
	if(VecTest.size() % 2 == 0){
		cout << MidSearch << endl;
		median = (VecTest.at(0) + VecTest.at(VecTest.size() - 1)) / 2;
		return median;
	}else {
		cout << MidSearch << endl;
		MidSearch = MidSearch  + 1;
		cout << MidSearch << endl;
		return VecTest.at(MidSearch);
	}
}

int Mode(vector<int> VecTest){ // Find Mode
	int Numcheck[101] = {};	//Big - O : N^3
	int temp;
	int temp_index;
	for(int i = 0 ; i < VecTest.size() ; i++){
		Numcheck[VecTest.at(i)]++;
	}
	for(int j = 0 ; j < 101 ; j++){
		temp = Numcheck[j];
		int k = j-1;
		while(k >= 0 && Numcheck[j] > temp){
			Numcheck[j-1] = Numcheck[j];
			k--;
		} 
		Numcheck[j-1] = temp;
	}
	return Numcheck[0];
}

int StandardDeviation(vector<int> VecTest){ // Find SD 
	int MEAN = mean(VecTest);	//Big - O : N
	int SD = 0;
	for(int i = 0 ; i < VecTest.size() ; i++){
		SD = SD + (VecTest.at(i) - MEAN);
		pow(SD , 2);
	}
	SD = SD / VecTest.size();
	return sqrt(SD);
}

int OddNumber (vector<int> VecTest){ // Find how many of odd number
	int Odd = 0;					//Big - O : N
	for(int i = 0 ; i < VecTest.size() ; i++){
		if(VecTest.at(i) % 2 != 0){
			Odd++;
		}
	}
	return Odd;
}

int EvenNumber(vector<int> VecTest){ // Find How many of Even number
	int Even = 0;					// Big - O : N
	for(int i = 0 ; i < VecTest.size() ; i++){
		if(VecTest.at(i) % 2 == 0){
			Even++;
		}
	}
	return Even;
}

void COUTVec(vector<int> VecTest){ // just cout 
	for(int i = 0 ; i < VecTest.size() ; i++){ // Big - O : N
		cout << setw(3) << VecTest.at(i) << " ";
		if( i % 10 == 0) {
			cout << endl;
		}
	}
}
